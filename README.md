## Arkitektur

Løsningen er lavet i .NET Core 3.1, og består af følgende projekter:

- Website (ASP.NET Core MVC) med simpelt layout til at illustrere et scoreboard
- Core (.NET Core Class Library) med interfaces.
- BowlingGame (ASP.NET Core Class Library) med implementation af 'TenPinBowling'.
- BowlingGame.Tests (.NET Core NUnit Test Project) med en række tests med forskellige scenarier.

Der er anvendt en abstrakt klasse i form af BaseFrame, som indeholder to statiske metoder til oprettelse af konkrete implementationer af Frame: StrikeFrame, SpareFrame, Frame og FinalFrame. 
Hver enkelt implementation har ansvaret for beregning af bonuspoint.

I websitet injectes en instans af TenPinBowling vha. Dependency Injection, og det er derved nemt at skifte til en anden implementation af interfacet IBowlingGame.

---

## Begrænsninger og mangler

- Der understøttes kun én spiller.
- Det er ikke muligt at vise scoreboard for et spil, som ikke er fuldendt.
- Scoreboardet er endnu ikke interaktivt, og er dermed et statisk billede af de angivne delresultater i HomeController/Index
- Styling bør laves i LESS eller SASS og bør ligeledes transpileres vha. eksempelvis Grunt