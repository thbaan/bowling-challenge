using System.Linq;
using NUnit.Framework;

namespace BowlingGame.Tests
{
    public class Tests
    {
        private TenPinBowling _tenPinBowling;

        [SetUp]
        public void Setup()
        {
            _tenPinBowling = new TenPinBowling();
        }

        [Test]
        public void TestGutterGameTotalScore()
        {
            _tenPinBowling.Restart();

            int[] rolls = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            foreach (var pins in rolls)
            {
                _tenPinBowling.Roll(pins);
            }

            var score = _tenPinBowling.Score();

            Assert.AreEqual(0, score);
        }

        [Test]
        public void TestPerfectGameTotalScore()
        {
            _tenPinBowling.Restart();

            int[] rolls = { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };

            foreach (var pins in rolls)
            {
                _tenPinBowling.Roll(pins);
            }

            var score = _tenPinBowling.Score();

            Assert.AreEqual(300, score);
        }

        [Test]
        public void TestAllOnes()
        {
            for (var i = 0; i < 20; i++)
            {
                _tenPinBowling.Roll(1);
            }

            Assert.AreEqual(20, _tenPinBowling.Score());
        }

        [Test]
        public void TestFirstFrameSpareOthersRollingAllTwos()
        {
            _tenPinBowling.Roll(9);
            _tenPinBowling.Roll(1);

            for (var i = 0; i < 18; i++)
            {
                _tenPinBowling.Roll(2);
            }

            Assert.AreEqual(48, _tenPinBowling.Score());
        }

        [Test]
        public void TestFirstTwoFramesSparesOthersAllRollTwos()
        {
            _tenPinBowling.Roll(5);
            _tenPinBowling.Roll(5);

            _tenPinBowling.Roll(5);
            _tenPinBowling.Roll(5);

            for (var i = 0; i < 16; i++)
            {
                _tenPinBowling.Roll(2);
            }

            Assert.AreEqual(59, _tenPinBowling.Score());
        }

        [Test]
        public void TestFirstFrameStrikeOthersRollAllTwos()
        {
            _tenPinBowling.Roll(10);

            for (var i = 0; i < 18; i++)
            {
                _tenPinBowling.Roll(2);
            }

            Assert.AreEqual(50, _tenPinBowling.Score());
        }

        [Test]
        public void TestFirstTwosFramesStrikeOthersRollAllTwos()
        {
            _tenPinBowling.Roll(10);
            _tenPinBowling.Roll(10);

            for (var i = 0; i < 16; i++)
            {
                _tenPinBowling.Roll(2);
            }

            Assert.AreEqual(68, _tenPinBowling.Score());
        }

        [Test]
        public void TestPartlyGame1()
        {
            _tenPinBowling.Roll(1);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(4);
            _tenPinBowling.Roll(5);

            _tenPinBowling.Roll(6);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(5);
            _tenPinBowling.Roll(4);

            Assert.AreEqual(38, _tenPinBowling.Score());
        }

        [Test]
        public void TestPartlyGame2()
        {
            _tenPinBowling.Roll(1);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(4);
            _tenPinBowling.Roll(5);

            _tenPinBowling.Roll(6);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(5);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(10);

            _tenPinBowling.Roll(0);
            _tenPinBowling.Roll(1);

            Assert.AreEqual(50, _tenPinBowling.Score());

        }

        [Test]
        public void TestPartlyGame3()
        {
            _tenPinBowling.Roll(1);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(4);
            _tenPinBowling.Roll(5);

            _tenPinBowling.Roll(6);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(5);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(10);

            _tenPinBowling.Roll(0);
            _tenPinBowling.Roll(1);

            _tenPinBowling.Roll(7);
            _tenPinBowling.Roll(3);

            _tenPinBowling.Roll(6);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(9);
            _tenPinBowling.Roll(0);

            Assert.AreEqual(94, _tenPinBowling.Score());

        }

        [Test]
        public void TestLikeInTaskExample()
        {
            _tenPinBowling.Roll(1);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(4);
            _tenPinBowling.Roll(5);

            _tenPinBowling.Roll(6);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(5);
            _tenPinBowling.Roll(5);

            _tenPinBowling.Roll(10);

            _tenPinBowling.Roll(0);
            _tenPinBowling.Roll(1);

            _tenPinBowling.Roll(7);
            _tenPinBowling.Roll(3);

            _tenPinBowling.Roll(6);
            _tenPinBowling.Roll(4);

            _tenPinBowling.Roll(10);

            _tenPinBowling.Roll(2);
            _tenPinBowling.Roll(8);
            _tenPinBowling.Roll(6);

            Assert.AreEqual(133, _tenPinBowling.Score());

        }

        [Test]
        public void TestScoreboardLikeInTask()
        {
            _tenPinBowling.Restart();

            int[] rolls = { 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6 };

            foreach (var pins in rolls)
            {
                _tenPinBowling.Roll(pins);
            }

            var score = _tenPinBowling.ScoreBoard();

            int[] expectedScores = { 5, 9, 15, 20, 11, 1, 16, 20, 20, 16 };

            var realScores = score.GetFrames().Select(x => x.Score()).ToArray();

            Assert.AreEqual(expectedScores, realScores);
        }

        [Test]
        public void TestScoreboard()
        {
            _tenPinBowling.Restart();

            int[] rolls = { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };

            foreach (var pins in rolls)
            {
                _tenPinBowling.Roll(pins);
            }

            var score = _tenPinBowling.ScoreBoard();

            int[] expectedScores = { 30, 30, 30, 30, 30, 30, 30, 30, 30, 30 };

            var realScores = score.GetFrames().Select(x => x.Score()).ToArray();

            Assert.AreEqual(expectedScores, realScores);
        }
    }
}