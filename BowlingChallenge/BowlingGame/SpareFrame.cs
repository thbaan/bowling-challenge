﻿using Core.Interfaces;

namespace BowlingGame
{
    public class SpareFrame : BaseFrame
    {
        public SpareFrame(int firstRoll, int secondRoll) : base(firstRoll, secondRoll) { }

        public override void AddBonus(IFrame framePlusOne, IFrame framePlusTwo)
        {
            _frameScore += framePlusOne.FirstRoll();
        }
    }
}
