﻿using Core.Interfaces;

namespace BowlingGame
{
    public class FinalFrame : BaseFrame
    {
        private readonly int _pinsThirdRoll;

        public int ThirdRoll() { return _pinsThirdRoll; }
        
        public FinalFrame(int pinsFirstRoll, int pinsSecondRoll, int pinsThirdRoll) : base(pinsFirstRoll, pinsSecondRoll)
        {
            _pinsThirdRoll = pinsThirdRoll;
        }

        public override void AddBonus(IFrame framePlusOne, IFrame framePlusTwo)
        {
            _frameScore += _pinsThirdRoll;
        }

    }
}
