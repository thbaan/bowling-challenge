﻿using Core.Interfaces;

namespace BowlingGame
{
    public class StrikeFrame : BaseFrame
    {
        public StrikeFrame() : base(10, 0) { }

        public override void AddBonus(IFrame framePlusOne, IFrame framePlusTwo)
        {
            if (framePlusOne is StrikeFrame)
                _frameScore += 10 + framePlusTwo.FirstRoll();
            else
                _frameScore += framePlusOne.FirstRoll() + framePlusOne.SecondRoll();
        }
    }
}
