﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Interfaces;

namespace BowlingGame
{
    public class TenPinBowling : IBowlingGame
    {
        private readonly List<IFrame> _frames;
        private readonly List<int> _rolls;

        public TenPinBowling()
        {
            _frames = new List<IFrame>();
            _rolls = new List<int>();
        }

        public void Roll(int pins)
        {
            Validate(pins);

            _rolls.Add(pins);

            if (IsFinalFrame)
            {
                HandleFinalFrame(pins);
            }

            else if (IsStrike(pins))
            {
                HandleStrike();
            }
            
            else if (!IsFirstRollInFrame)
            {
                HandleFrame();
            }
        }

        private void HandleFrame()
        {
            _frames.Add(BaseFrame.Create(_rolls[_rolls.Count - 2], _rolls[_rolls.Count - 1]));
            _rolls.Clear();
        }

        private bool IsFirstRollInFrame => _rolls.Count <= 1;

        private void HandleStrike()
        {
            _frames.Add(BaseFrame.Create(10, 0));
            _rolls.Clear();
        }

        private void HandleFinalFrame(int pins)
        {
            if (RollsInFinalFrame == 2)
            {
                // Second roll in final frame is not a strike or a spare
                if (!IsStrike(pins) && !IsSpare)
                {
                    _frames.Add(BaseFrame.Create(_rolls[_rolls.Count - 2], _rolls[_rolls.Count - 1], 0));
                }
            }

            if (RollsInFinalFrame == 3)
            {
                // Third roll in final frame
                _frames.Add(BaseFrame.Create(_rolls[_rolls.Count - 3], _rolls[_rolls.Count - 2], _rolls[_rolls.Count - 1]));
            }
        }

        private static bool IsStrike(in int pins) => pins == 10;
        private bool IsSpare => _rolls[0] + _rolls[1] == 10;
        private bool IsFinalFrame => _frames.Count == 9;

        private bool IsGameFinished => _frames.Count >= 10;

        private int RollsInFinalFrame => _rolls.Count;

        private void Validate(int pins)
        {
            if (IsGameFinished)
                throw new Exception("Game over");

            if (pins > 10 || pins < 0)
                throw new ArgumentOutOfRangeException();
        }

        public int Score()
        {
            var frames = _frames;

            
            if (_frames.Count == 10)
            {
                frames.Add(BaseFrame.Create(0, 0));
                frames.Add(BaseFrame.Create(0, 0));

                for (var i = 0; i < 10; i++)
                    frames[i].AddBonus(frames[i + 1], frames[i + 2]);
            }
            else
            {
                frames.Add(BaseFrame.Create(0, 0));
                frames.Add(BaseFrame.Create(0, 0));
                frames.Add(BaseFrame.Create(0, 0));

                for (var i = 0; i < frames.Count - 2; i++)
                    frames[i].AddBonus(frames[i + 1], frames[i + 2]);
            }

            var counter = 0;
            frames.ForEach(frame => counter += frame.Score());
            return counter;
        }

        public IScoreBoard ScoreBoard()
        {
            var frames = _frames;

            // TODO
            frames.Add(BaseFrame.Create(0, 0));
            frames.Add(BaseFrame.Create(0, 0));

            for (var i = 0; i < 10; i++)
                frames[i].AddBonus(frames[i + 1], frames[i + 2]);

            // TODO
            frames.Remove(frames.Last());
            frames.Remove(frames.Last());

            return new ScoreBoard(frames);
        }

        public void Restart()
        {
            _rolls.Clear();
            _frames.Clear();
        }
    }
}
