﻿using System.Collections.Generic;
using Core.Interfaces;

namespace BowlingGame
{
    public class ScoreBoard : IScoreBoard
    {
        private readonly List<IFrame> _frames;

        public ScoreBoard(List<IFrame> frames)
        {
            _frames = frames;
        }

        public List<IFrame> GetFrames() => _frames;
    }
}
