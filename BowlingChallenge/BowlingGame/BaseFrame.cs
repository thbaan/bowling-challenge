﻿using Core.Interfaces;

namespace BowlingGame
{
    public abstract class BaseFrame : IFrame
    {
        protected int _pinsFirstRoll;
        protected int _pinsSecondRoll;
        protected int _frameScore;

        protected BaseFrame(int pinsFirstRoll, int pinsSecondRoll)
        {
            _pinsFirstRoll = pinsFirstRoll;
            _pinsSecondRoll = pinsSecondRoll;
        }

        public int Score() { return _pinsFirstRoll + _pinsSecondRoll + _frameScore; }

        public int FirstRoll() { return _pinsFirstRoll; }

        public int SecondRoll() { return _pinsSecondRoll; }

        public static BaseFrame Create(int firstRoll, int secondRoll)
        {
            if (firstRoll == 10)
                return new StrikeFrame();

            if (firstRoll + secondRoll == 10)
                return new SpareFrame(firstRoll, secondRoll);

            return new Frame(firstRoll, secondRoll);
        }

        public static BaseFrame Create(int firstRoll, int secondRoll, int thirdRoll)
        {
            return new FinalFrame(firstRoll, secondRoll, thirdRoll);
        }

        public virtual void AddBonus(IFrame framePlusOne, IFrame framePlusTwo) { }
    }
}
