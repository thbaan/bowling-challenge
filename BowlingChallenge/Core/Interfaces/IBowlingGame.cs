﻿using System.Collections.Generic;

namespace Core.Interfaces
{
    public interface IBowlingGame
    {
        void Roll(int pins);
        int Score();
        IScoreBoard ScoreBoard();
    }

    public interface IScoreBoard
    {
        List<IFrame> GetFrames();
    }

    public interface IFrame
    {
        int Score();
        int FirstRoll();
        int SecondRoll();
        void AddBonus(IFrame framePlusOne, IFrame framePlusTwo);

    }


}
