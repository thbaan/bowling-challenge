using System.Collections.Generic;
using System.Linq;
using Core.Interfaces;

namespace Website.Models
{
    public class PlayerViewModel
    {
        public string Name { get; set; }
        public List<IFrame> Frames { get; set; }
        public int TotalScore => Frames.Sum(x => x.Score());
    }
}
