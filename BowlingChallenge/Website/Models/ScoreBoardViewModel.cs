using System;
using System.Collections.Generic;

namespace Website.Models
{
    public class ScoreBoardViewModel
    {
        public List<PlayerViewModel> Players { get; set; }
    }
}
