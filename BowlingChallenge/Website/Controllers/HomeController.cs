﻿using System.Collections.Generic;
using System.Diagnostics;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Website.Models;

namespace Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBowlingGame _bowlingGame;

        public HomeController(IBowlingGame bowlingGame)
        {
            _bowlingGame = bowlingGame;
        }

        public IActionResult Index()
        {
            _bowlingGame.Roll(0);
            _bowlingGame.Roll(9);

            _bowlingGame.Roll(5);
            _bowlingGame.Roll(0);

            _bowlingGame.Roll(3);
            _bowlingGame.Roll(5);

            _bowlingGame.Roll(3);
            _bowlingGame.Roll(1);

            _bowlingGame.Roll(6);
            _bowlingGame.Roll(1);

            _bowlingGame.Roll(4);
            _bowlingGame.Roll(3);

            _bowlingGame.Roll(6);
            _bowlingGame.Roll(0);

            _bowlingGame.Roll(6);
            _bowlingGame.Roll(3);

            _bowlingGame.Roll(6);
            _bowlingGame.Roll(0);

            _bowlingGame.Roll(7);
            _bowlingGame.Roll(1);


            var viewModel = new ScoreBoardViewModel
            {
                Players = new List<PlayerViewModel>
                {
                    new PlayerViewModel
                    {
                        Name = "Channon",
                        Frames = _bowlingGame.ScoreBoard().GetFrames()
                    }
                }
            };

            return View(viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
